module.exports = {
  printWidth: 120,
  semi: false,
  singleQuote: true,
  trailingComma: "none",

  importOrder: [
    // First, external libraries
    // Project files
    '^@/(.*)$',
    // Tests files
    '^%/(.*)$',
    // Relative TS files
    '^[./].*(?<!\\.vue)$',
    // Relative vue files
    '^[./].*(\\.vue)$'
  ],
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,

}
