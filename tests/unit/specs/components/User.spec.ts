import { mount } from '@vue/test-utils'

import User from '@/components/User.vue'
import { Team } from '@/data/users'

const user = { name: 'Arnaud', initials: 'ALG', pictureUrl: 'arnaud', active: true, team: Team.SaRA }

describe('User.vue', () => {
  test('mount component', async () => {
    expect(User).toBeTruthy()

    const wrapper = mount(User, { props: { user: user } })

    expect(wrapper.element).toMatchSnapshot()
  })
})
