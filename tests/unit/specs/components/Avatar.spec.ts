import { mount } from '@vue/test-utils'

import Avatar from '@/components/Avatar.vue'

describe('Avatar.vue', () => {
  test('mount component with image', async () => {
    expect(Avatar).toBeTruthy()

    const wrapper = mount(Avatar, { props: { initials: 'ALG', pictureUrl: 'arnaud' } })

    expect(wrapper.element).toMatchSnapshot()
  })

  test('mount component without image', async () => {
    expect(Avatar).toBeTruthy()

    const wrapper = mount(Avatar, { props: { initials: 'ALG' } })

    expect(wrapper.element).toMatchSnapshot()
  })
})
