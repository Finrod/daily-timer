import { mount } from '@vue/test-utils'

import End from '@/views/End.vue'

vi.setSystemTime(1645635606596)
localStorage.getItem = vi.fn().mockImplementation(() => String(1645635606596 - 47000))

describe('End.vue', () => {
  test('mount component', async () => {
    expect(End).toBeTruthy()

    const wrapper = mount(End)

    expect(wrapper.element).toMatchSnapshot()
  })
})
