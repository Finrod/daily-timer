import { mount } from '@vue/test-utils'

import Home from '@/views/Home.vue'

vi.mock('@/data/users', () => ({
  AVATAR_COLORS: [],
  getTeam: vi.fn().mockReturnValue('cloud-riders'),
  getUsers: vi.fn().mockReturnValue([
    { name: 'Riri', avatar: 'Riri', active: true, team: 'cloud-riders', starter: 0 },
    { name: 'Fifi', avatar: 'Fifi', active: true, team: 'cloud-riders', starter: 0 },
    { name: 'Loulou', avatar: 'Loulou', active: true, team: 'cloud-riders', starter: 0 }
  ])
}))

describe('Home.vue', () => {
  afterEach(() => {
    vi.restoreAllMocks()
  })

  test('mount component', async () => {
    expect(Home).toBeTruthy()

    const wrapper = mount(Home)

    expect(wrapper.element).toMatchSnapshot()
  })
})
