import { mount } from '@vue/test-utils'

import Settings from '@/views/Settings.vue'

describe('Settings.vue', () => {
  test('mount component', async () => {
    expect(Settings).toBeTruthy()

    const wrapper = mount(Settings)

    expect(wrapper.element).toMatchSnapshot()
  })
})
