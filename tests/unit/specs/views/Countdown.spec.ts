import { flushPromises, mount } from '@vue/test-utils'

import Countdown from '@/views/Countdown.vue'

vi.mock('lodash-es', () => ({
  shuffle: vi.fn().mockImplementation((array: string[]) => array)
}))

vi.mock('@/data/users', () => ({
  saveStarter: vi.fn(),
  getTeam: vi.fn().mockReturnValue('cloud-riders'),
  getUsers: vi.fn().mockReturnValue([
    { name: 'Riri', avatar: 'Riri', active: true, team: 'cloud-riders', starter: 0 },
    { name: 'Fifi', avatar: 'Fifi', active: true, team: 'cloud-riders', starter: 0 },
    { name: 'Loulou', avatar: 'Loulou', active: true, team: 'cloud-riders', starter: 0 }
  ])
}))

describe('Countdown.vue', () => {
  test('mount component', async () => {
    expect(Countdown).toBeTruthy()

    const wrapper = mount(Countdown)
    await flushPromises()

    expect(wrapper.element).toMatchSnapshot()
  })
})
