// Import vue test utils functions and stubs
import { config } from '@vue/test-utils'

// Global stubs
const RouterLinkStub = { template: '<a class="router-link"><slot /></a>' }
const RouterViewStub = { template: '<div class="router-view" />' }

// Define plugins, mocks, stubs...
config.global.stubs = {
  RouterLink: RouterLinkStub,
  RouterView: RouterViewStub
}
