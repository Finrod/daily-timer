/// <reference types="vitest" />
import vue from '@vitejs/plugin-vue'
import { defineConfig } from 'vite'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  server: { port: 8090 },

  resolve: { alias: { '@': '/src' } },

  base: process.env.NODE_ENV === 'production' ? '/daily-timer/' : '/',

  plugins: [
    vue(),
    VitePWA({
      // Assets from public dir
      includeAssets: ['favicon.svg', 'favicon.ico', 'robots.txt', 'apple-touch-icon.png', 'avatars/*.png'],
      // Global assets from src
      workbox: {
        globPatterns: ['**/*.{js,css,html,jpg,png}']
      },
      // PWA Manifest
      manifest: {
        name: 'Daily Timer',
        short_name: 'Daily Timer',
        description: 'Official Daily timer of the Clourd Riders',
        orientation: 'portrait',
        theme_color: '#212121',
        background_color: '#212121',
        icons: [
          {
            src: 'pwa-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'pwa-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          },
          {
            src: 'pwa-512x512.png',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'any'
          },
          {
            src: 'pwa-512x512-splash.png',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'maskable'
          }
        ]
      }
    })
  ],

  test: {
    globals: true,
    environment: 'happy-dom',
    setupFiles: ['tests/unit/tests-setup.ts'],

    coverage: {
      all: true,
      include: ['src'],
      exclude: ['**/*.d.ts', 'src/main.ts', 'src/App.vue', 'src/router.ts'],

      provider: 'c8',
      reporter: ['text-summary', 'html'],
      reportsDirectory: './tests/unit/coverage'
    }
  }
})
