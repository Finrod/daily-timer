module.exports = {
  root: true,

  env: {
    browser: true,
    es2021: true,
    node: true
  },

  extends: [
    'plugin:vue/vue3-recommended',
    'eslint:recommended',
    '@vue/typescript/recommended',
    'plugin:prettier/recommended'
  ],

  parserOptions: {
    ecmaVersion: 'latest'
  },

  rules: {
    // Deactivated rules
    'vue/multi-word-component-names': 'off',

    // Custom rules from uncategorized category
    'vue/component-name-in-template-casing': ['error', 'PascalCase'],
    'vue/match-component-file-name': [
      'error',
      {
        extensions: ['js', 'jsx', 'ts', 'tsx', 'vue'],
        shouldMatchCase: true
      }
    ],
    'sort-imports': [
      'error',
      {
        ignoreDeclarationSort: true,
        allowSeparatedGroups: true
      }
    ]
  }
}
