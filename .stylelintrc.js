module.exports = {
  extends: [
    'stylelint-config-standard-scss',
    'stylelint-config-recess-order',
    'stylelint-config-standard-vue/scss'
  ],

  rules: {
    // Give weird errors
    'no-descending-specificity': null
  },

  ignoreFiles: ['**/*.html']
}
