import { RouteRecordRaw, createRouter, createWebHashHistory } from 'vue-router'

import Countdown from '@/views/Countdown.vue'
import End from '@/views/End.vue'
import Home from '@/views/Home.vue'
import Settings from '@/views/Settings.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings
  },
  {
    path: '/countdown',
    name: 'countdown',
    component: Countdown
  },
  {
    path: '/end',
    name: 'end',
    component: End
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
