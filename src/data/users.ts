import { isEqual, orderBy } from 'lodash-es'

export enum Team {
  SaRA = 'sara',
  Scar = 'scar',
  Valinor = 'valinor',
  FrontEnd = 'front-end',
  // Wallix 1 for SaRA and Valinor: same daily
  WallixOne = 'wallix-one'
}

export interface User {
  name: string
  initials: string
  avatar?: string
  active: boolean
  team: Team
}

export type UserStatus = '' | 'warning' | 'alert' | 'dead' | 'disabled'

export const users: User[] = [
  // Cloud riders - SCAR
  { name: 'Arnaud', initials: 'ALG', active: true, team: Team.Scar, avatar: 'arnaud' },
  { name: 'Lucas', initials: 'LP', active: true, team: Team.Scar, avatar: 'lucas' },
  { name: 'Pauline', initials: 'PB', active: true, team: Team.Scar, avatar: 'pauline' },
  { name: 'Sylvain', initials: 'SF', active: true, team: Team.Scar, avatar: 'sylvain' },
  { name: 'Yann', initials: 'YS', active: true, team: Team.Scar, avatar: 'yann' },
  { name: 'Yoënn', initials: 'YB', active: false, team: Team.Scar, avatar: 'yoenn' },
  // Cloud riders - Wallix One
  { name: 'Arnaud', initials: 'ALG', active: true, team: Team.WallixOne, avatar: 'arnaud' },
  { name: 'Baptiste', initials: 'BR', active: true, team: Team.WallixOne, avatar: 'baptiste' },
  { name: 'Florian', initials: 'FG', active: true, team: Team.WallixOne, avatar: 'florian' },
  { name: 'Gauthier', initials: 'GH', active: true, team: Team.WallixOne, avatar: 'gauthier' },
  { name: 'Gwen', initials: 'GF', active: true, team: Team.WallixOne },
  { name: 'Hakan', initials: 'HM', active: true, team: Team.WallixOne, avatar: 'hakan' },
  { name: 'Kévin', initials: 'KG', active: true, team: Team.WallixOne, avatar: 'kevin_g' },
  { name: 'Liam', initials: 'LQ', active: true, team: Team.WallixOne },
  { name: 'Lucas', initials: 'LP', active: true, team: Team.WallixOne, avatar: 'lucas' },
  { name: 'Mikaël', initials: 'MF', active: true, team: Team.WallixOne, avatar: 'mikael' },
  { name: 'Pauline', initials: 'PB', active: true, team: Team.WallixOne, avatar: 'pauline' },
  { name: 'Sylvain', initials: 'SF', active: true, team: Team.WallixOne, avatar: 'sylvain' },
  { name: 'Yann', initials: 'YS', active: true, team: Team.WallixOne, avatar: 'yann' },
  // Front-end
  { name: 'Arthur', initials: 'AR', active: true, team: Team.FrontEnd },
  { name: 'Caroline', initials: 'CP', active: true, team: Team.FrontEnd },
  { name: 'Ilhan', initials: 'IY', active: true, team: Team.FrontEnd },
  { name: 'Mathieu', initials: 'MC', active: true, team: Team.FrontEnd },
  { name: 'Solenn', initials: 'SM', active: true, team: Team.FrontEnd },
  { name: 'Sylvain', initials: 'SF', active: true, team: Team.FrontEnd }
]

const LS_KEY_USERS = 'dt_users'
const LS_KEY_TEAM = 'dt_team'

export function getUsers(): User[] {
  const localUsers = localStorage.getItem(LS_KEY_USERS)

  if (localUsers) {
    const savedUsers = JSON.parse(localUsers) as User[]
    const savedUsernames = savedUsers.map((u) => u.name).sort()

    const baseUsernames = users.map((u) => u.name).sort()

    if (isEqual(savedUsernames, baseUsernames)) return orderBy(savedUsers, ['name'], ['asc'])
  }

  // Return default list if list content changes
  return users
}

export function saveUsers(users: User[]): void {
  localStorage.setItem(LS_KEY_USERS, JSON.stringify(users))
}

export function clearUsers(): void {
  localStorage.removeItem(LS_KEY_USERS)
  localStorage.removeItem(LS_KEY_TEAM)
}

export function getTeam(): Team {
  return (localStorage.getItem(LS_KEY_TEAM) as Team) || Team.WallixOne
}

export function saveTeam(team: Team): void {
  localStorage.setItem(LS_KEY_TEAM, team)
}
