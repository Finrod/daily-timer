export const DEFAULT_DURATION = 90
export const LS_DURATION_KEY = 'daily_duration'

// Format duration to mm:ss
export function formatDuration(duration: number) {
  if (isNaN(duration)) return '-'

  const minutes = Math.floor(duration / 60)
  const seconds = duration - minutes * 60

  let formattedDuration = ''
  if (minutes) formattedDuration += `${minutes}:`
  formattedDuration += minutes && seconds < 10 ? `0${seconds}` : `${seconds}`

  return formattedDuration
}

// Save duration in local storage
export function saveDuration(duration: number) {
  localStorage.setItem(LS_DURATION_KEY, `${duration}`)
}

// Get duration from local storage
export function getDuration() {
  return localStorage.getItem(LS_DURATION_KEY) || DEFAULT_DURATION
}
